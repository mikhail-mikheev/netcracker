package edu.nectracker.application.info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InfoServiceImpl implements InfoService {

    @Autowired
    private Info info;

    public String sendEducation() {
        return info.getEducation();
    }

    public String sendWork() {
        return info.getWork();
    }

    public String sendHobbies() {
        return info.getHobbies();
    }
}
