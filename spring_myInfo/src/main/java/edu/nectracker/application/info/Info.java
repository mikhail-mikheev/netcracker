package edu.nectracker.application.info;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class Info {

    @Value("${info.education}")
    private String education;

    @Value("${info.work}")
    private String work;

    @Value("${info.hobbies}")
    private String hobbies;

    public String getEducation(){
        return this.education;
    }

    public String getHobbies() {
        return hobbies;
    }

    public String getWork() {
        return work;
    }
}
