package edu.nectracker.application.info;

import org.springframework.stereotype.Component;

@Component
public interface InfoService {
    public String sendEducation();
    public String sendWork();
    public String sendHobbies();
}
