package edu.nectracker.application.controller;

import edu.nectracker.application.info.InfoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @Autowired
    private InfoServiceImpl infoService;

    @GetMapping("/education")
    public String getEducation() {
        return infoService.sendEducation();
    }

    @GetMapping("/work")
    public String getWork() {
        return infoService.sendWork();
    }

    @GetMapping("/hobbies")
    public String getHobbies() {
        return infoService.sendHobbies();
    }
}