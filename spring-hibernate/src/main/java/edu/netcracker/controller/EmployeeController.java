package edu.netcracker.controller;

import edu.netcracker.entity.Employee;
import edu.netcracker.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/add")
    public Employee createEmployee(
            @RequestParam String name,
            @RequestParam Long departmentId,
            @RequestParam List<Long> phoneIds) {
        return employeeService.createEmployee(name, departmentId, phoneIds);
    }

    @GetMapping("/all")
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }
}
