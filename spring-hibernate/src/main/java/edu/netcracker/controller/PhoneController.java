package edu.netcracker.controller;

import edu.netcracker.entity.Phone;
import edu.netcracker.service.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/phone")
public class PhoneController {

    @Autowired
    private PhoneService phoneService;

    @GetMapping()
    public List<Phone> getPhonesByIds (@RequestParam List<Long> ids){
        return phoneService.getPhonesByIds(ids);
    }

    @PostMapping("/add")
    public Phone addPhone (@RequestParam String phoneNumber) {
        return phoneService.addPhone(phoneNumber);
    }

}
