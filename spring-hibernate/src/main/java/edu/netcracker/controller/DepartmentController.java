package edu.netcracker.controller;

import edu.netcracker.entity.Department;
import edu.netcracker.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/department")
public class DepartmentController{

    @Autowired
    private DepartmentService departmentService;

    @GetMapping()
    public Department getDepartmentById(@RequestParam Long id){
        return departmentService.getDepartmentById(id);
    }

    @PostMapping("/add")
    public Department addDepartment (@RequestParam String name) {
        return departmentService.addDepartment(name);
    }
}

