package edu.netcracker.service;

import edu.netcracker.entity.Phone;
import java.util.List;

public interface PhoneService {

    Phone addPhone(String phoneNumber);

    List<Phone> getPhonesByIds(List<Long> ids);
}
