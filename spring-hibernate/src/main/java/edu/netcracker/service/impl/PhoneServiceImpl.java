package edu.netcracker.service.impl;

import edu.netcracker.entity.Phone;
import edu.netcracker.repository.PhoneRepository;
import edu.netcracker.service.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhoneServiceImpl implements PhoneService {

    @Autowired
    private PhoneRepository phoneRepository;

    @Override
    public Phone addPhone(String phoneNumber) {
        Phone phone = new Phone(phoneNumber);
        phoneRepository.save(phone);
        return phone;
    }

    @Override
    public List<Phone> getPhonesByIds(List<Long> ids) {
        return phoneRepository.findAllById(ids);
    }
}
