package edu.netcracker.service.impl;

import edu.netcracker.entity.Department;
import edu.netcracker.repository.DepartmentRepository;
import edu.netcracker.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Department addDepartment(String name) {
        Department department = new Department(name);
        departmentRepository.save(department);
        return department;
    }

    @Override
    public Department getDepartmentById(Long id) {
        return departmentRepository.findById(id).orElse(null);
    }
}


