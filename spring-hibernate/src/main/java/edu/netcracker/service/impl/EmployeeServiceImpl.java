package edu.netcracker.service.impl;

import edu.netcracker.entity.Department;
import edu.netcracker.entity.Employee;
import edu.netcracker.entity.Phone;
import edu.netcracker.repository.DepartmentRepository;
import edu.netcracker.repository.EmployeeRepository;
import edu.netcracker.repository.PhoneRepository;
import edu.netcracker.service.DepartmentService;
import edu.netcracker.service.EmployeeService;
import edu.netcracker.service.PhoneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private PhoneService phoneService;

    @Override
    public Employee createEmployee(String name, Long departmentId, List<Long> phoneIds) {
        Department department = departmentService.getDepartmentById(departmentId);
        List<Phone> phones = phoneService.getPhonesByIds(phoneIds);
        Employee employee = new Employee(name, department, phones);
        employeeRepository.save(employee);
        return employee;
    }

    @Override
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }
}
