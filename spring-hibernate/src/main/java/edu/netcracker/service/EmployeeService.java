package edu.netcracker.service;

import edu.netcracker.entity.Employee;

import java.util.List;

public interface EmployeeService {

    Employee createEmployee(String name, Long departmentId, List<Long> phoneIds);

    List<Employee> getAllEmployees();
}
