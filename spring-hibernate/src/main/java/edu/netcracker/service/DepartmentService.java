package edu.netcracker.service;

        import edu.netcracker.entity.Department;

public interface DepartmentService {

    Department addDepartment(String name);

    Department getDepartmentById(Long id);
}
